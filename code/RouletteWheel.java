import java.util.Random;

public class RouletteWheel {
    private Random randomizer;
    private int lastSpinNum;
    public RouletteWheel(){
        this.randomizer=new Random();
        this.lastSpinNum=0;
    }
    public void spin() {
        this.lastSpinNum = randomizer.nextInt(0, 38);
    }
    public int getValue(){
        return this.lastSpinNum;
    }

}
